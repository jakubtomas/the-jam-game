const stepsTemplate = [...Array(16).fill(false)];

export const drums = {
  kick: stepsTemplate,
  snare: stepsTemplate,
  tom: stepsTemplate
};
