import React, { useEffect, useContext } from "react";
import styled from "styled-components";
import { string, arrayOf, bool, func } from "prop-types";

import drums from "../../samplers/drums";
import { capitalizeFirstLetter } from "../../lib";
import { ClockContext } from "../../lib/clockContext";

const Container = styled.div`
  display: flex;
  align-items: center;
`;

const SampleInfo = styled.div`
  width: 5rem;
`;

const StepBlock = styled.div`
  padding: 0.5rem;
  background: ${({ step, index }) => (step === index ? "red" : "blue")};
`;

const Sequencer = ({ type, instr, steps, updateSteps }) => {
  const step = useContext(ClockContext);

  useEffect(() => {
    if (steps[step]) recognizeAndPlay();
  }, [step]);

  const recognizeAndPlay = () => {
    if (type === "drums") drums.get(instr).start();
  };

  return (
    <Container>
      <SampleInfo>{capitalizeFirstLetter(instr)}</SampleInfo>
      {steps.map((checked, index) => (
        <StepBlock index={index} key={index} step={step}>
          <input
            type="checkbox"
            onChange={() => updateSteps(instr, index)}
            checked={checked}
          />
        </StepBlock>
      ))}
    </Container>
  );
};

Sequencer.propTypes = {
  type: string.isRequired,
  instr: string.isRequired,
  steps: arrayOf(bool).isRequired,
  updateSteps: func.isRequired
};

export default Sequencer;
