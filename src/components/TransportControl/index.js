import React, { Fragment } from "react";
import { bool, func, number } from "prop-types";

const TransportControl = ({ play, playPause, resetClock, bpm, changeBpm }) => {
  return (
    <Fragment>
      <button onClick={playPause}>{play ? "Pause" : "Play"}</button>
      <button onClick={resetClock}>Stop</button>
      {"BPM: "}
      <input type="number" value={bpm} onChange={changeBpm} />
    </Fragment>
  );
};

TransportControl.propTypes = {
  play: bool.isRequired,
  playPause: func.isRequired,
  resetClock: func.isRequired,
  bpm: number.isRequired,
  changeBpm: func.isRequired
};

export default TransportControl;
