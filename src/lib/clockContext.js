import { createContext } from "react";

export const ClockContext = createContext();
export const ClockContextProvider = ClockContext.Provider;
