import Tone from "tone";

const drumSamples = new Tone.Players(
  {
    kick: "samples/drums/kick.mp3",
    snare: "samples/drums/snare.wav",
    tom: "samples/drums/tom.wav"
  },
  () => console.log("Drums loaded")
).toMaster();

export default drumSamples;
