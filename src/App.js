import React, { useState, useEffect, useRef } from "react";

import { ClockContextProvider } from "./lib/clockContext";

import DrumMachine from "./instruments/DrumMachine/";
import TransportControl from "./components/TransportControl";

const App = () => {
  const [play, setPlay] = useState(false);
  const [bpm, setBpm] = useState(190);
  const [step, setStep] = useState(-1);
  const tempoInMs = useRef(60000 / bpm / 4);

  useEffect(() => {
    if (step === 16) setStep(0);

    tempoInMs.current = 60000 / bpm / 4;
  }, [step, bpm]);

  const increaseStep = () => {
    const iterator = () => {
      setStep(prevStep => prevStep + 1);

      window.clock = setTimeout(() => iterator(), tempoInMs.current);
    };

    iterator();
  };

  const playPause = () =>
    play
      ? (setPlay(false), clearTimeout(window.clock))
      : (setPlay(true), increaseStep());

  const resetClock = () =>
    play
      ? (setStep(-1), setPlay(false), clearTimeout(window.clock))
      : setStep(-1);

  const changeBpm = event => {
    const { value } = event.target;

    value <= 240 && value >= 40 && setBpm(value);
  };

  return (
    <ClockContextProvider value={step}>
      <DrumMachine />
      <TransportControl
        play={play}
        playPause={playPause}
        resetClock={resetClock}
        bpm={bpm}
        changeBpm={changeBpm}
      />
    </ClockContextProvider>
  );
};

export default App;
