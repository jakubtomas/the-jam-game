import React from "react";
import styled from "styled-components";

const Container = styled.div`
  margin: 0.5rem 0;
  display: flex;
`;

const Heading = styled.div`
  width: 5rem;
`;

const RadioButton = styled.input`
  margin: 0 1rem;
`;

const BarsSelector = ({ bars, changeBars }) => {
  return (
    <Container>
      <Heading>Bars</Heading>
      <div onChange={event => changeBars(Number(event.target.value))}>
        <RadioButton
          type="radio"
          name="bars"
          value="1"
          defaultChecked={bars === 1}
        />
        1
        <RadioButton
          type="radio"
          name="bars"
          value="2"
          defaultChecked={bars === 2}
        />
        2
        <RadioButton
          type="radio"
          name="bars"
          value="3"
          defaultChecked={bars === 3}
        />
        3
        <RadioButton
          type="radio"
          name="bars"
          value="4"
          defaultChecked={bars === 4}
        />
        4
      </div>
    </Container>
  );
};

export default BarsSelector;
