import React, { useState } from "react";
import styled from "styled-components";

import { drums } from "../../constants";

import Sequencer from "../../components/Sequencer";
import BarsSelector from "./BarsSelector";

const Container = styled.div`
  margin: 1rem;
`;

const DrumMachine = () => {
  const [instruments, updateInstruments] = useState(drums);
  const [bars, setBars] = useState(1);

  const makeProps = (instr, steps) => ({
    instr,
    steps,
    updateSteps,
    type: "drums"
  });

  const updateSteps = (instrument, index) => {
    let newSteps = [...instruments[instrument]];
    newSteps[index] = !newSteps[index];

    updateInstruments({ ...instruments, [instrument]: newSteps });
  };

  const changeBars = newBars => {
    let newInstruments = { ...instruments };

    Object.keys(newInstruments).map(instr => {
      if (newBars === 1) {
        newInstruments[instr].length = 16;
      }

      if (newBars > 1 && newBars > bars) {
        const barsToUpdate = newBars - bars;

        newInstruments[instr] = newInstruments[instr].concat([
          ...Array(16 * barsToUpdate).fill(false)
        ]);
      }

      if (newBars > 1 && newBars < bars) {
        const barsToUpdate = bars - (bars - newBars);

        newInstruments[instr].length = 16 * barsToUpdate;
      }

      setBars(newBars);
      updateInstruments(newInstruments);
      return newBars;
    });
  };

  const { kick, snare, tom } = instruments;

  return (
    <Container>
      <BarsSelector bars={bars} changeBars={changeBars} />
      <Sequencer {...makeProps("kick", kick)} />
      <Sequencer {...makeProps("snare", snare)} />
      <Sequencer {...makeProps("tom", tom)} />
    </Container>
  );
};

export default DrumMachine;
